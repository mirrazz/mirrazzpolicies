# Mirrazz Privacy Policy

Here we document the collection and use of your data. We categorize by parts of the Service.

## Account Creation
When you create a Mirrazz account, you are required to provide the following information:
* **Username** This is public. It is used to log into your account, and allows other users to communicate with you.
* **Email address** This is not public. Your email address is only used for password resets and, should you choose to enable it, two-factor authentication (2FA).
* **Password** Allows you to log in. If you think someone has your password, you may want to change it.

## Mirrazz Storage
When you use Mirrazz Storage, we collect the following data:
* **Files** Files uploaded are stored on Mirrazz, and heavily encrypted. With certain file types, metadata will be split.
* **File Share Data** Data collected when you share files. This is a list of usernames and permissions required to allow others to acces your file.
* **File Access Dates** Mirrazz allows you to view the creation and modification dates of files.

## Mirrazz Rooms
Mirrazz Rooms is our chatting system. Here's the data we collect when you use it:
* **Messages** Messages need to be stored in order for users to see them.
* **Typing status** We temporarily store if you're typing to display to other users. You can disable this at any time.
* **Users in Group DMs** This allows users to acces Group DMs if they're in it. This also shows a list of users in the Group DM.
* **DM List** This shows a list of people you've sent a Direct Message too, or people that've sent you a Direct Message. This allows you to easily get back to a conversation.
* **Profile/Bio** In Mirrazz Rooms, you can enter a Bio(About Me) that will display when people look at your profile.
* **Status** You can set your status to Online, Idle, or Do Not Disturb. If you're not on Mirrazz Rooms, your status will be set to Offline,
* **Structures You're In** This allows you to access Structures you've joined.
* **Structure Settings** This makes it easier to find, moderate, and manage Structures.

## Mirrazz Analytics
Mirrazz Analytics is our open-source privacy-perserving analytics service. It collects limited data, which is listed here:
* **User-Agent** A user-agent is a piece of information your browser sends that contains information about your browser, operating system, and device hardware. This is used to determine the popularity of sites across different platforms, for example, if there are more mobile than desktop users, or more people use it on Internet Explorer 5 than Chrome 100 (yes, it works in IE5. Lots of our things do.)
* **The Site Domain Name** This allows us to link it to the right site, so analytics don't get confused with each other.
* **The Current Page's Path** This is used to determine the popularity of pages. For example, a news sight could see that their "China Bans Milk" article is more popular than their "The Earth is Flat" article (both fake news, \<insert epic troll here\>)

> :information: Note: Mirrazz Analytics respects Do Not Track (DNT) headers. Neither the script or the server will allow analytics when Do Not Track is enabled.

## MirrazzAI
MirrazzAI only makes requests to Mirrazz if you log into your Mirrazz account. All text-to-speech and dictation(speech recognition) is done locally on the device.
MirrazzAI may use third-party services to get information, such as showing you the weather.
Here is a list of all third-party services MirrazzAI uses.
* **OpenWeatherMap** Open weather map is the most privacy-perserving way to get live weather information. They don't log requests sent to their API. They only use analytics on their main website.

Yep, that's it!

## MirrazzOS
MirrazzOS only makes requests to Mirrazz if you log into your Mirrazz account. Each profile can be linked to one Mirrazz account at a time.
MirrazzOS itself doesn't use any third-party services, however, MirrazzOS does include MirrazzAI.
MirrazzAI may use third-party services to get information, such as traffic or the weather.<br>
For a list of third-party services MirrazzAI uses, refer to the MirrazzAI section (which is conveiniently above you!)
MirrazzOS comes shipped with Mozilla Firefox as it's default browser. Please read [Firefox's Privacy Policy](https://www.mozilla.org/en-US/privacy/firefox/) to see how Firefox handle's your data.

## The Mirrazz Website
Mirrazz's website uses privacy-perserving analytics, which are self-hosted. We also use our own service, Mirrazz Analytics.
We do not have any analytics on the login page or account creation page.<br><br>
We use opt-in analytics on Mirrazz Storage and Mirrazz Rooms.
By default, these analytics are enabled.
If you would like, you can enable these analytics
